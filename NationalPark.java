import java.util.Scanner;
public class NationalPark 
{
	public static void main(String[] args) 
	{
		Scanner get = new Scanner(System.in);
		Monkey[] troop = new Monkey[4];
	
		for(int i = 0; i < troop.length; i++)
			{
			troop[i] = new Monkey();
			System.out.println("Enter a type of monkey " + (i+1) + ": ");
			troop[i].type = get.nextLine();
			System.out.println("Does monkey " + (i+1) + " have a tail: ");
			troop[i].hasTail = get.nextLine();
			System.out.println("Enter a color of monkey " + (i+1) + ": ");
			troop[i].color = get.nextLine();
			}
		System.out.println("MONKEY 4: Type: " + troop[3].type + ", Has tail: " + troop[3].hasTail + ", Color: " + troop[3].color);
		troop[0].peelBanana();
		troop[0].swingFromTree();
	}
}
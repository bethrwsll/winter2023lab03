public class Monkey {
	public String type;
	public String hasTail;
	public String color;

	public void peelBanana(){
		System.out.println(this.type + " has peeled a banana!(which has suddenly disappeared...)");
	}
	public void swingFromTree(){
		if(this.hasTail.equals("yes")) {
			System.out.println(this.type + " is swinging through the treets with tail!");
		}
		else 
		{
			System.out.println(this.type + " is swinging through the trees with hands and feet!");
		}
	}
}